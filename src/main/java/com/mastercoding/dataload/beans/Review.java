package com.mastercoding.dataload.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Review {
    private String review_id;
    private String user_id;
    private String business_id;
    private float stars;
    private String date;
    private String text;
    private boolean usful;
    private boolean funny;
    private boolean cool;
    private String type;

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(String business_id) {
        this.business_id = business_id;
    }

    public float getStars() {
        return stars;
    }

    public void setStars(float stars) {
        this.stars = stars;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isUsful() {
        return usful;
    }

    public void setUsful(boolean usful) {
        this.usful = usful;
    }

    public boolean isFunny() {
        return funny;
    }

    public void setFunny(boolean funny) {
        this.funny = funny;
    }

    public boolean isCool() {
        return cool;
    }

    public void setCool(boolean cool) {
        this.cool = cool;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "Review{" +
                "review_id='" + review_id + '\'' +
                ", user_id='" + user_id + '\'' +
                ", business_id='" + business_id + '\'' +
                ", stars=" + stars +
                ", date='" + date + '\'' +
                ", text='" + text + '\'' +
                ", usful=" + usful +
                ", funny=" + funny +
                ", cool=" + cool +
                ", type='" + type + '\'' +
                '}';
    }
}
