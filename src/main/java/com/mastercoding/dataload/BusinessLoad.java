package com.mastercoding.dataload;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastercoding.dataload.beans.Address;
import com.mastercoding.dataload.beans.Business;
import com.mastercoding.dataload.beans.Review;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

public class BusinessLoad {
    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mastercoding", "root", "mastercoding")) {
            Class.forName("com.mysql.jdbc.Driver");
//            loadAddress(con);
//            loadBusinesses(con);
            loadReviews(con);
        }

    }

    private static void loadReviews(Connection connection) throws SQLException, IOException {

        try (FileReader reader = new FileReader("C:\\personal\\mastercoding\\mastercoding\\Dataset\\yelp_dataset_challenge_round9\\yelp_academic_dataset_review.json");
             BufferedReader buffReader = new BufferedReader(reader)) {
            String line = "";
            int i = 0;
            while ((line = buffReader.readLine()) != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Review review = mapper.readValue(line, Review.class);
                    String sql = "insert into review(business_id, review_id, stars, text, type) values(?, ?, ?, ?, ?)";
                    PreparedStatement reviewStatement = connection.prepareStatement(sql);
                    reviewStatement.setString(1, review.getBusiness_id());
                    reviewStatement.setString(2, review.getReview_id());
                    reviewStatement.setDouble(3, review.getStars());
                    reviewStatement.setString(4, review.getText());
                    reviewStatement.setString(5, review.getType());
                    System.out.println(review);
                    reviewStatement.executeUpdate();
                } catch (Exception e) {
                    System.out.println("review = " + e.getMessage());
                }
            }
        }

    }

    private static void loadBusinesses(Connection connection) throws IOException {
        try (FileReader reader = new FileReader("C:\\personal\\mastercoding\\mastercoding\\Dataset\\yelp_dataset_challenge_round9\\yelp_academic_dataset_business.json");
             BufferedReader buffReader = new BufferedReader(reader)) {
            String line = "";
            int i = 0;
            while ((line = buffReader.readLine()) != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Business business = mapper.readValue(line, Business.class);
                    Address address = mapper.readValue(line, Address.class);
                    System.out.println(business);
                    PreparedStatement addressStatement = connection.prepareStatement("select address_id from address where city = ? and state =? and postal_code=?");
                    addressStatement.setString(1, address.getCity());
                    addressStatement.setString(2, address.getState());
                    addressStatement.setString(3, address.getPostal_code());
                    ResultSet resultSet = addressStatement.executeQuery();
                    String sql = "insert into business(business_id, name, neighborhood, stars, type, address_id) values(?, ?, ?, ?, ?, ?)";
                    PreparedStatement businessStatement = connection.prepareStatement(sql);
                    businessStatement.setString(1, business.getBusiness_id());
                    businessStatement.setString(2, business.getName());
                    businessStatement.setString(3, business.getNeighborhood());
                    businessStatement.setDouble(4, business.getStars());
                    businessStatement.setString(5, business.getType());
                    if (resultSet != null && resultSet.next()) {
                        long addressId = resultSet.getLong(1);
                        businessStatement.setLong(6, addressId);
                    } else {
                        businessStatement.setString(6, null);
                    }
                    businessStatement.executeUpdate();
                } catch (Exception e) {
                    System.out.println("business = " + e.getMessage());
                }
            }
        }
    }

    private static void loadAddress(Connection connection) throws IOException, SQLException {
        try (FileReader reader = new FileReader("C:\\personal\\mastercoding\\mastercoding\\Dataset\\yelp_dataset_challenge_round9\\yelp_academic_dataset_business.json");
             BufferedReader buffReader = new BufferedReader(reader)) {
            String line = "";
            while ((line = buffReader.readLine()) != null) {
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    Address address = mapper.readValue(line, Address.class);
                    System.out.println(address);
                    String sql = "insert into address(city, state, postal_code, latitude, longitude) values(?, ?, ?, ?, ?)";
                    PreparedStatement statement = connection.prepareStatement(sql);
                    statement.setString(1, address.getCity());
                    statement.setString(2, address.getState());
                    statement.setString(3, address.getPostal_code());
                    statement.setDouble(4, address.getLatitude());
                    statement.setDouble(5, address.getLongitude());
                    statement.executeUpdate();
                    System.out.println(sql);
                } catch (Exception e) {
                    System.out.println("address = " + e.getMessage());
                }
            }
        }
    }

    private void loadData() throws ClassNotFoundException, SQLException {
    }
}